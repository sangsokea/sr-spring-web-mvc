package com.kshrd.srspringmvc.service.serviceImp;

import com.kshrd.srspringmvc.model.Student;
import com.kshrd.srspringmvc.repository.StudentRepository;
import com.kshrd.srspringmvc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.getAllStudent();
    }

    @Override
    public Student findStudentByID(int id) {
        return studentRepository.findStudentByID(id);
    }

    @Override
    public void studentMethod() {
        System.out.println("Version one the student method.");
    }

    @Override
    public void AddStudentMethod(Student student) {
        studentRepository.addNewStudent(student);
    }
}
