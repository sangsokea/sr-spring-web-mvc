package com.kshrd.srspringmvc.service;


import com.kshrd.srspringmvc.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {

    public List<Student> getAllStudents();
    public Student findStudentByID(int id );
    public void studentMethod();
    public void AddStudentMethod(Student student);



}
