package com.kshrd.srspringmvc.repository;

import com.github.javafaker.Faker;
import com.kshrd.srspringmvc.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student> students = new ArrayList<>();
   public StudentRepository(){
        // add some value to the students (list )

        for (int i =0 ; i<10 ; i++){
            Student student = new Student();
            Faker faker = new Faker();
            student.setId(i+1);
            student.setName(faker.name().fullName());
            student.setBirthdate("11/11");
            student.setProfile("http://localhost:8080/images/52617d26-32f8-4035-9e5a-37382929031d.jpg");
            student.setGender((i%2==0)?"Male":"Female");
            student.setAddress(faker.address().fullAddress());
            students.add(student);
        }
    }



  public   List<Student> getAllStudent(){

        return students;
    }

    public void addNewStudent(Student student){
       // 0
       student.setId(students.size()+1);
       students.add(student);
    }

    public Student findStudentByID(int id ){
       return students.stream().filter(student -> student.getId()== id).findFirst().orElseThrow();
    }
}
